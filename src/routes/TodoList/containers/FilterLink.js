import { connect } from "dva";
import { namespace } from "../../../models/todo-list/consts";
import { setVisibilityFilter } from "../../../models/todo-list/actions";
import Link from "../components/Link";

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state[namespace].visibilityFilter,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => dispatch(setVisibilityFilter(ownProps.filter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Link);
