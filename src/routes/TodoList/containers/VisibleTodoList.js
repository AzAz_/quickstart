import { connect } from "dva";
import { VisibilityFilters, namespace } from "../../../models/todo-list/consts";
import { toggleTodo } from "../../../models/todo-list/actions";
import TodoList from "../components/TodoList";

const getVisibleTodos = (todos = [], filter) => {
  switch (filter) {
    case VisibilityFilters.ALL:
      return todos;
    case VisibilityFilters.COMPLETED:
      return todos.filter((t) => !t.active);
    case VisibilityFilters.ACTIVE:
      return todos.filter((t) => t.active);
    default:
      throw new Error("Unknown filter: " + filter);
  }
};

const mapStateToProps = (state) => {
  return {
    todos: getVisibleTodos(
      state[namespace].todos,
      state[namespace].visibilityFilter
    ),
  };
};

const mapDispatchToProps = (dispatch) => ({
  toggleTodo: (id) => dispatch(toggleTodo(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
