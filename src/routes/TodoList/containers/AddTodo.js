import React from "react";
import { Form, Input, Button } from "antd";
import { connect } from "dva";
import { addTodo, addAfter1Second } from "../../../models/todo-list/actions";

const mapDispatchToProps = (dispatch) => {
  return {
    addTodo: (val) => {
      dispatch(addTodo(val));
    },
    addAfter1Second: (val) => {
      dispatch(addAfter1Second(val));
    },
  };
};

const AddTodo = ({ addTodo, addAfter1Second }) => {
  let input = {};
  return (
    <div>
      <Form
        name="basic"
        initialValues={{ remember: true }}
        onFinish={(e) => {
          if (!input.value.trim()) {
            return;
          }
          addTodo(input.value);
          input.value = "";
        }}
      >
        <Form.Item
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input
            style={{ width: 300 }}
            onChange={(e) => {
              input.value = e.target.value;
            }}
          />
          <Button type="primary" htmlType="submit">
            Add Todo
          </Button>
        </Form.Item>
      </Form>
      <Button
        type="primary"
        onClick={() => addAfter1Second("abc" + Math.random())}
      >
        1s后添加
      </Button>
    </div>
  );
};

export default connect(null, mapDispatchToProps)(AddTodo);
