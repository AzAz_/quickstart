import React from "react";
import FilterLink from "../containers/FilterLink";
import { VisibilityFilters } from "../../../models/todo-list/consts";

const Footer = () => (
  <div>
    <span>Show: </span>
    <FilterLink filter={VisibilityFilters.ALL}>All</FilterLink>
    <FilterLink filter={VisibilityFilters.ACTIVE}>Active</FilterLink>
    <FilterLink filter={VisibilityFilters.COMPLETED}>Completed</FilterLink>
  </div>
);

export default Footer;
