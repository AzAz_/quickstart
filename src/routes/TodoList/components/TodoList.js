import React from "react";
import Todo from "./Todo";

const TodoList = ({ todos, toggleTodo }) => {
  return (
    <ul>
      {todos.map((todo, i) => (
        <Todo
          key={i}
          {...todo}
          completed={todo.completed || false}
          onClick={() => toggleTodo(todo.id)}
        />
      ))}
    </ul>
  );
};


export default TodoList;
