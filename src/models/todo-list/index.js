import { namespace, initialState } from "./consts";
import { addTodo, ADD_TODO } from "./actions";
import reducers from "./reducers";
export default {
  namespace,
  state: initialState,

  subscriptions: {
    setup({ dispatch, history }) {
      dispatch(addTodo("first init, oh yeah ~"));
      return history.listen(({ pathname }) => {
        dispatch(addTodo(`${pathname} visited ~`));
      });
    },
  },

  effects: {
    *addAfter1Second(action, { call, put }) {
      // yield call(delay, 1000);
      yield put({ ...action, type: ADD_TODO });
    },
  },
  reducers,
};
