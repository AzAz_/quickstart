import { cloneObj } from "./helpers";

function add(state = {}, action) {
  state = cloneObj(state);
  state.todos = [...state.todos, { id: action.id, text: action.text }];
  return state;
}
function toggle(state, action) {
  state = cloneObj(state);
  state.todos = state.todos.map((todo) => {
    if (todo.id === action.id) {
      todo.active = !todo.active;
    }
    return todo;
  });
  return state;
}
function setVisibilityFilter(state = {}, action) {
  state = cloneObj(state);
  state.visibilityFilter = action.visibilityFilter;
  return state;
}
export default {
  add,
  toggle,
  setVisibilityFilter,
};
