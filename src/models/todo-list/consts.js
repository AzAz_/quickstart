export const namespace = "todoList";

export const VisibilityFilters = {
  ALL: "all",
  COMPLETED: "Completed",
  ACTIVE: "Active",
};

export const initialState = {
  todos: [
    {
      id: 0,
      text: "a",
      active: true,
    },
    {
      id: 1,
      text: "b",
      active: true,
    },
    {
      id: 2,
      text: "c",
      active: true,
    },
  ],
  visibilityFilter: VisibilityFilters.ALL,
};
