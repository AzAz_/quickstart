import { namespace } from "./consts";
import { wrapNamespace } from "./helpers";
let nextTodoId = 100;
export const ADD_TODO = wrapNamespace("add", namespace);
export const TOGGLE_TODO = wrapNamespace("toggle", namespace);
export const SET_VISIBILITY_FILTER = wrapNamespace(
  "setVisibilityFilter",
  namespace
);
export const ADD_AFTER_1_SECOND = wrapNamespace("addAfter1Second", namespace);

const makeActionCreators = (type, ...argNames) => {
  return function (...args) {
    let action = { type };
    argNames.forEach(
      (argName, i) => (action[argName] = args[i] || nextTodoId++)
    );
    return action;
  };
};

export const addTodo = makeActionCreators(ADD_TODO, "text", "id", "active");
export const toggleTodo = makeActionCreators(TOGGLE_TODO, "id");
export const setVisibilityFilter = makeActionCreators(
  SET_VISIBILITY_FILTER,
  "visibilityFilter"
);
export const addAfter1Second = makeActionCreators(
  ADD_AFTER_1_SECOND,
  "text",
  "id",
  "active"
);
