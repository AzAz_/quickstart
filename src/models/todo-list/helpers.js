export const wrapNamespace = (type, namespace) => `${namespace}/${type}`;
export const cloneObj = (obj) => JSON.parse(JSON.stringify(obj));
