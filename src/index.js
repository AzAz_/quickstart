import dva from "dva";
import { browserHistory } from "react-router";
import { initialState as todoListInitialState } from "./models/todo-list/consts";
// import createLoading from "dva-loading";
import "./index.css";

// 1. Initialize
const app = dva({
  initialState: {
    todoList: Object.assign({}, todoListInitialState),
  },
  history: browserHistory,
});

// 2. Plugins
// app.use({});

// 3. Model
app.model(require("./models/example").default);
app.model(require("./models/todo-list").default);
// 4. Router
app.router(require("./router").default);

// 5. Start
app.start("#root");
